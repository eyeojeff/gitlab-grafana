# gitlab-grafana

This is a small POC that allows you to graph CICD Pipeline run duration in Grafana using the gitlab API.
Bizarrely there is no native graph which shows this in gitlab. This project provides a HTTP endpoint 
compatible with the grafana [`simpod-json-datasource`](https://grafana.com/grafana/plugins/simpod-json-datasource) plugin. At the moment it only shows successful
jobs.

## Configuration

The environment variables `GITLAB_SERVER` and `GITLAB_TOKEN` need to be set to allow API requests to your gitlab instance.

## Grafana Installation
Grafana needs to be deployed with the `simpod-json-datasource` plugin, so something like...

```
docker run -d \
  -p 3000:3000 \
  --name=grafana \
  -e "GF_INSTALL_PLUGINS=simpod-json-datasource" \
  grafana/grafana
```