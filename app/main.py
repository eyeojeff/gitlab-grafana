#! /usr/bin/env python3
import os
import gitlab
from calendar import timegm
from datetime import datetime
from flask import Flask, request, jsonify

app = Flask(__name__)
app.debug = True

gl = gitlab.Gitlab(
    os.environ["GITLAB_SERVER"].strip(),
    private_token=os.environ["GITLAB_TOKEN"].strip(),
    # ssl_verify=False,
)


def convert_to_time_ms(timestamp):
    # 2019-12-06T15:25:09.107+01:00
    return 1000 * timegm(
        datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%f%z").timetuple()
    )


def getgitlabdata(projectids):
    data = []

    for projectid in projectids:
        project = gl.projects.get(projectid)

        projectdata = {
            "target": project.attributes["name_with_namespace"],
            "datapoints": [],
        }

        for job in project.jobs.list(per_page=1000):
            if not job.attributes["duration"] \
                    or job.attributes["status"] != "success":
                continue

            projectdata["datapoints"].append(
                [
                    job.attributes["duration"],
                    convert_to_time_ms(job.attributes["started_at"]),
                ]
            )

        data.append(projectdata)

    return data


@app.route("/")
def health_check():
    return "This datasource is healthy."


@app.route("/search", methods=["POST"])
def search():
    projects = gl.projects.list(starred=True)
    response = []
    for p in projects:
        if p.attributes["archived"]:
            continue
        response.append(
            {"text": p.attributes["name_with_namespace"],
             "value": p.attributes["id"]}
        )
    return jsonify(response)


@app.route("/query", methods=["POST"])
def query():
    req = request.get_json()
    projectids = [x["target"] for x in req["targets"]]
    data = getgitlabdata(projectids=projectids)
    return jsonify(data)


# @app.route('/annotations', methods=['POST'])
# def annotations():
#     req = request.get_json()
#     data = [
#         {
#             "annotation": 'This is the annotation',
#             "time": (convert_to_time_ms(req['range']['from']) +
#                      convert_to_time_ms(req['range']['to'])) / 2,
#             "title": 'Deployment notes',
#             "tags": ['tag1', 'tag2'],
#             "text": 'Hm, something went wrong...'
#         }
#     ]x
#     return jsonify(data)


# @app.route('/tag-keys', methods=['POST'])
# def tag_keys():
#     data = [
#         {"type": "string", "text": "City"},
#         {"type": "string", "text": "Country"}
#     ]
#     return jsonify(data)


# @app.route('/tag-values', methods=['POST'])
# def tag_values():
#     req = request.get_json()
#     if req['key'] == 'City':
#         return jsonify([
#             {'text': 'Tokyo'},
#             {'text': 'São Paulo'},
#             {'text': 'Jakarta'}
#         ])
#     elif req['key'] == 'Country':
#         return jsonify([
#             {'text': 'China'},
#             {'text': 'India'},
#             {'text': 'United States'}
#         ])


if __name__ == "__main__":
    try:
        port = int(os.environ["LISTEN_PORT"].strip())
    except KeyError:
        port = 5000
    app.run(host="0.0.0.0", port=port)
