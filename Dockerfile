FROM python:3

RUN pip install --upgrade pip && \
    pip install --no-cache-dir python-gitlab flask requests

ADD app/ /app
CMD [ "/app/main.py" ]